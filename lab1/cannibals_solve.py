initial_state = {'m': 3, 'c': 3, 'b': 1}


def apply(hstate, action):
    new_hstate = {'state': {}, 'history': hstate['history'][:]}

    for key in hstate['state'].keys():
        new_hstate['state'][key] = hstate['state'][key] + action[key] if hstate['state']['b'] == 0 else hstate['state'][
                                                                                                            key] - \
                                                                                                        action[key]

    new_hstate['history'].append(new_hstate['state'])
    return new_hstate


def satisfies_constraints(state):
    a = state['m'] >= 0 and state['c'] >= 0
    b = 3 - state['m'] >= 0 and 3 - state['c'] >= 0
    c = state['m'] == 0 or state['m'] >= state['c']
    d = 3 - state['m'] == 0 or 3 - state['m'] >= 3 - state['c']
    return a and b and c and d


possible_actions = [{'m': 1, 'c': 0, 'b': 1},
                    {'m': 2, 'c': 0, 'b': 1},
                    {'m': 0, 'c': 1, 'b': 1},
                    {'m': 0, 'c': 2, 'b': 1},
                    {'m': 1, 'c': 1, 'b': 1}]


def find_solution(initial_state):
    hstates = [{'state': initial_state, 'history': [initial_state]}]

    while hstates:
        next_hstates = []

        for hstate in hstates:
            for action in possible_actions:
                new_hstate = apply(hstate, action)
                if satisfies_constraints(new_hstate['state']):
                    next_hstates.append(new_hstate)

                if new_hstate['state'] == {'m': 0, 'c': 0, 'b': 0}:
                    return new_hstate['history']

        hstates = next_hstates

    return None


def print_history(history):
    for state in history:
        m0 = 'M' * state['m']
        c0 = 'C' * state['c']
        b = ' B | ' if state['b'] == 1 else ' | B '
        m1 = 'M' * (3 - state['m'])
        c1 = 'C' * (3 - state['c'])
        print(f"{m0}{c0}{b}{m1}{c1}")


print_history(find_solution(initial_state))