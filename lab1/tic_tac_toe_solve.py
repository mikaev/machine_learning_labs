AI1 = False
AI2 = True

X = 'X'
O = 'O'
EMPTY = '.'
SIZE = 3

initial_state = [[EMPTY for x in range(SIZE)] for y in range(SIZE)]


def opposite(player):
    return O if player == X else X


def has_won(state, player):
    for y in range(SIZE):
        win_row = True
        for x in range(SIZE):
            if state[y][x] != player:
                win_row = False
                break
        if win_row:
            return True

    for x in range(SIZE):
        win_col = True
        for y in range(SIZE):
            if state[y][x] != player:
                win_col = False
                break
        if win_col:
            return True

    win_diag = True
    for i in range(SIZE):
        if state[i][i] != player:
            win_diag = False
            break
    if win_diag:
        return True

    win_diag = True
    for i in range(SIZE):
        if state[SIZE - i - 1][i] != player:
            win_diag = False
            break
    if win_diag:
        return True

    return False


def available_spots(state):
    spots = []
    for y in range(SIZE):
        for x in range(SIZE):
            if state[y][x] == EMPTY:
                spots.append([x, y])
    return spots


def minimax(state, player):
    if has_won(state, X):
        return {'move': None, 'score': 1}
    elif has_won(state, O):
        return {'move': None, 'score': -1}

    spots = available_spots(state)
    if not spots:
        return {'move': None, 'score': 0}

    moves = []
    for spot in spots:
        state[spot[1]][spot[0]] = player
        score = minimax(state, opposite(player))['score']
        state[spot[1]][spot[0]] = EMPTY
        moves.append({'move': spot, 'score': score})

    bestMoveIndex = 0
    for i, move in enumerate(moves):
        better = None
        if player == X:
            better = move['score'] > moves[bestMoveIndex]['score']
        else:
            better = move['score'] < moves[bestMoveIndex]['score']

        if better:
            bestMoveIndex = i

    return moves[bestMoveIndex]


def print_state(state):
    for y in range(SIZE):
        for x in range(SIZE):
            print(state[y][x], end='')
        print()


def is_valid(state, move):
    for spot in available_spots(state):
        if spot == move:
            return True
    return False


def main():
    state = initial_state
    player = X
    while True:
        print_state(state)
        print()
        opp = opposite(player)
        if has_won(state, opp):
            print(f"{opp} has won!")
            break
        elif not available_spots(state):
            print("Draw!")
            break

        while True:
            mx = None
            my = None

            if player == X:
                if AI1:
                    aiMove = minimax(state, player)['move']
                    mx = aiMove[0]
                    my = aiMove[1]
                else:
                    inp = input()
                    print()
                    i = int(inp) - 1
                    mx = i % SIZE
                    my = i // SIZE
            else:
                if AI2:
                    aiMove = minimax(state, player)['move']
                    mx = aiMove[0]
                    my = aiMove[1]
                else:
                    inp = input()
                    print()
                    i = int(inp) - 1
                    mx = i % SIZE
                    my = i // SIZE

            if is_valid(state, [mx, my]):
                break
            else:
                print("Illegal move :O\n")

        state[my][mx] = player
        player = opp


main()