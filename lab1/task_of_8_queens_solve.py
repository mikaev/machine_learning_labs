N = 8


def get_solutions():
    boards = [[[False for x in range(N)] for y in range(N)]]

    for y in range(N):
        next_boards = []
        for board in boards:
            for x in range(N):
                queen_fits = True

                for xx in [-1, 0, 1]:
                    for i in range(1, N):
                        nx = x + xx * i
                        ny = y - i

                        if nx >= 0 and nx < N and ny >= 0 and ny < N:
                            if (board[ny][nx]):
                                queen_fits = False
                                break
                        else:
                            break

                    if not queen_fits:
                        break

                if queen_fits:
                    board_with_fit_queen = [row[:] for row in board]
                    board_with_fit_queen[y][x] = True
                    next_boards.append(board_with_fit_queen)

        boards = next_boards

    return boards


solution_count = len(get_solutions())

print(f"{solution_count} solutions have been found for the {N} x {N} board")